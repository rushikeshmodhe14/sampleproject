import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.*;

public class CheckValidateMobileNo {
    static List<String> validExpressions = List.of("\\d{3}[-]\\d{3}[-]\\d{4}", "[(]\\d{3}[)][ ]\\d{3}[-]\\d{4}",
            "[(]\\d{3}[)][ ]\\d{3}[-]\\d{4}",
            "(\\+\\d{1}[ ])\\d{3}[ ]\\d{3}[ ]\\d{4}",
            "(\\+\\d{1}[ ])\\((\\d{3})\\)[ ](\\d{3})[-](\\d{4})",
            "(\\+\\d{1}[-])\\d{3}[-]\\d{3}[-]\\d{4}",
            "(\\+\\d{1}[-])\\(\\d{3}\\)\\d{3}[-]\\d{4}",
            "\\d{10}",
            "\\d{3}[ ]\\d{3}[ ]\\d{4}",
            "(\\+\\d{2})[ ]\\d{3}[ ]\\d{3}[-]\\d{4}",
            "(\\+\\d{2})[-]\\d{3}[-]\\d{3}[-]\\d{4}",
            "\\d{2}[-]\\(\\d{3}\\)(\\d{3})[-]\\d{4}");

    public static String validateMobileNo(String mobileNo) throws Exception {
        Pattern matchedPattern = null;
        for (String expression : validExpressions) {
            Pattern pattern = Pattern.compile(expression);
            Matcher matcher = pattern.matcher(mobileNo);
            if (matcher.matches()) {
                matchedPattern = pattern;
                break;
            }
        }
        if (matchedPattern == null) {
            throw new Exception("Invalid Mobile No");
        }
        String extractedMobileNo = "";
        int count = 0;//123-345-567
        int last = mobileNo.length() - 1;
        while (last >= 0) {
            if (mobileNo.charAt(last) >= '0' && mobileNo.charAt(last) <= '9') {
                extractedMobileNo = extractedMobileNo + mobileNo.charAt(last);
                count++;
            }
            if (count == 10) {
                break;
            }
            last--;
        }
        StringBuilder sb = new StringBuilder(extractedMobileNo);
        sb.reverse();
        return sb.toString();
    }

    // "354-555-4544", "(123) 866-2345", "234 876 2342", "+1 345 678 1222",
    // "+90 123 334-2342", "+92-764-090-2342", "91-(123)444-2342", and "3036284638"
    public static void main(String a[]) throws Exception {
        String userMobileNo = "3036284638";
        String validMobNo = validateMobileNo(userMobileNo);
        System.out.println("Extracted Mobile No " + validMobNo);
    }
}
/*
for 1 no
String userMobileNo ="354-555-4544";
Extracted Mobile No 3545554544
for 2 no
String userMobileNo ="(123) 866-2345";
Extracted Mobile No 1238662345
for 3 no
String userMobileNo ="234 876 2342";
Extracted Mobile No 2348762342
for 4 no
String userMobileNo ="+1 345 678 1222";
Extracted Mobile No 3456781222
for 5 no
String userMobileNo ="+90 123 334-2342";
Extracted Mobile No 1233342342
for 6 no
String userMobileNo ="+92-764-090-2342";
Extracted Mobile No 7640902342
for 7 no
String userMobileNo ="91-(123)444-2342";
Extracted Mobile No 1234442342
for 8 no
String userMobileNo ="3036284638";
Extracted Mobile No 3036284638
*/

